/*
Copyright (c) Perinet GmbH
All rights reserved

This software is dual-licensed: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License version 3 as
published by the Free Software Foundation. For the terms of this
license, see http://www.fsf.org/licensing/licenses/agpl-3.0.html,
or contact us at https://perinet.io/contact/ when you want license
this software under a commercial license.
*/

package clients_test

import (
	"log"
	"testing"
	"time"

	"gotest.tools/v3/assert"

	"gitlab.com/perinet/generic/lib/utils/shellhelper"

	ClientsProvider "gitlab.com/perinet/periMICA-container/apiservice/mosquitto/clients"
)

const (
	tmpDir           = "_cache/"
	mosquittoLogFile = tmpDir + "var/log/mosquitto.log"
)

func init() {
	ClientsProvider.SetConfig(ClientsProvider.Config{LogFilePath: mosquittoLogFile})
}

func TestEmpty(t *testing.T) {

	mosquittoLog := ``
	shellhelper.WriteFile(mosquittoLogFile, mosquittoLog)

	clients := ClientsProvider.GetClients()
	assert.Assert(t, len(clients) == 0)
}

func TestRestart(t *testing.T) {

	mosquittoLog := `
	2024-05-31T08:55:00: Client auto-6C16948F-A012-4FD3-5CA2-F81B577C3626 disconnected: Protocol error.
	2024-05-31T08:55:11: New connection from fe80::742e:db1d:d1b2:0:63507 on port 8883.
	2024-05-31T08:55:14: New client connected from fe80::742e:db1d:d1b2:0:63507 as auto-351ABCD3-36F4-3F74-C522-3BBB8E94A855 (p2, c1, k60).
	2024-05-31T09:00:51: Client auto-351ABCD3-36F4-3F74-C522-3BBB8E94A855 disconnected: Protocol error.
	2024-05-31T09:00:54: New connection from fe80::742e:db1d:d1b2:0:63508 on port 8883.
	2024-05-31T09:00:58: New client connected from fe80::742e:db1d:d1b2:0:63508 as auto-B3215F63-12CD-1469-6CD8-B95651BF8270 (p2, c1, k60).
	2024-05-31T09:07:59: mosquitto version 2.0.18 terminating
	2024-05-31T09:08:01: mosquitto version 2.0.18 starting
	2024-05-31T09:08:01: Config loaded from /etc/mosquitto/mosquitto.conf.
	2024-05-31T09:08:01: Opening websockets listen socket on port 8083.
	2024-05-31T09:08:01: Opening ipv6 listen socket on port 8883.
	2024-05-31T09:08:01: mosquitto version 2.0.18 running
	`
	shellhelper.WriteFile(mosquittoLogFile, mosquittoLog)
	time.Sleep(time.Millisecond * 1000)

	clients := ClientsProvider.GetClients()
	assert.Assert(t, len(clients) == 0)
}

func TestRestartEmpty(t *testing.T) {

	mosquittoLog := `
	2024-05-31T09:08:01: mosquitto version 2.0.18 starting
	2024-05-31T09:08:01: Config loaded from /etc/mosquitto/mosquitto.conf.
	2024-05-31T09:08:01: Opening websockets listen socket on port 8083.
	2024-05-31T09:08:01: Opening ipv6 listen socket on port 8883.
	2024-05-31T09:08:01: mosquitto version 2.0.18 running
	2024-05-31T09:08:01: New connection from fe80::742e:db1d:d1b2:0:63509 on port 8883.
	2024-05-31T09:08:03: New client connected from fe80::742e:db1d:d1b2:0:63509 as auto-85FC4C78-1503-FC9E-39E8-B878E6DB94AC (p2, c1, k60).	
	2024-05-31T09:08:01: New connection from fe80::742e:db1d:d1b2:0:63509 on port 8883.
	2024-05-31T09:08:03: New client connected from fe80::742e:db1d:d1b2:0:63509 as auto-85FC4C78-1503-FC9E-39E8-B878E6DB94AC (p2, c1, k60).	
	2024-05-31T09:08:01: mosquitto version 2.0.18 starting
	2024-05-31T09:08:01: Config loaded from /etc/mosquitto/mosquitto.conf.
	2024-05-31T09:08:01: Opening websockets listen socket on port 8083.
	2024-05-31T09:08:01: Opening ipv6 listen socket on port 8883.
	2024-05-31T09:08:01: mosquitto version 2.0.18 running
	`
	shellhelper.WriteFile(mosquittoLogFile, mosquittoLog)
	time.Sleep(time.Millisecond * 1000)

	clients := ClientsProvider.GetClients()
	assert.Assert(t, len(clients) == 0)
}

func TestRestart1(t *testing.T) {

	mosquittoLog := `
	2024-05-31T09:07:59: mosquitto version 2.0.18 terminating
	2024-05-31T09:08:01: mosquitto version 2.0.18 starting
	2024-05-31T09:08:01: Config loaded from /etc/mosquitto/mosquitto.conf.
	2024-05-31T09:08:01: Opening websockets listen socket on port 8083.
	2024-05-31T09:08:01: Opening ipv6 listen socket on port 8883.
	2024-05-31T09:08:01: mosquitto version 2.0.18 running
	2024-05-31T09:08:01: New connection from fe80::742e:db1d:d1b2:0:63509 on port 8883.
	2024-05-31T09:08:03: New client connected from fe80::742e:db1d:d1b2:0:63509 as auto-85FC4C78-1503-FC9E-39E8-B878E6DB94AC (p2, c1, k60).
	2024-05-31T09:23:05: Client auto-85FC4C78-1503-FC9E-39E8-B878E6DB94AC disconnected: Protocol error.
	2024-05-31T09:23:23: New connection from fe80::742e:db1d:d1b2:0:63510 on port 8883.
	2024-05-31T09:23:24: New client connected from fe80::742e:db1d:d1b2:0:63510 as auto-1EB8D368-5B81-CC22-FF39-9666354EA49A (p2, c1, k60).
	2024-05-31T09:25:33: Client auto-1EB8D368-5B81-CC22-FF39-9666354EA49A disconnected: Protocol error.
	2024-05-31T09:25:36: New connection from fe80::742e:db1d:d1b2:0:63511 on port 8883.
	2024-05-31T09:25:38: New client connected from fe80::742e:db1d:d1b2:0:63511 as auto-A347E158-330E-B9CA-4771-A2DA1CF3FC19 (p2, c1, k60).
	2024-05-31T09:43:09: Client auto-A347E158-330E-B9CA-4771-A2DA1CF3FC19 disconnected: Protocol error.
	2024-05-31T09:43:13: New connection from fe80::742e:db1d:d1b2:0:63512 on port 8883.
	2024-05-31T09:43:14: New client connected from fe80::742e:db1d:d1b2:0:63512 as auto-DF2261FA-747D-2BF6-AEF5-357DE3F4A4E4 (p2, c1, k60).
	2024-05-31T09:50:01: Client auto-DF2261FA-747D-2BF6-AEF5-357DE3F4A4E4 disconnected: Protocol error.
	2024-05-31T09:50:01: New connection from fe80::742e:db1d:d1b2:0:63513 on port 8883.
	2024-05-31T09:50:03: New client connected from fe80::742e:db1d:d1b2:0:63513 as auto-7A5E6B0B-F673-F2F9-CEEA-4BD51C2CFE45 (p2, c1, k60).
	2024-05-31T09:57:55: Client auto-7A5E6B0B-F673-F2F9-CEEA-4BD51C2CFE45 disconnected: Protocol error.
	2024-05-31T09:58:02: New connection from fe80::742e:db1d:d1b2:0:63514 on port 8883.	
	2024-05-31T09:07:59: mosquitto version 2.0.18 terminating
	2024-05-31T09:08:01: mosquitto version 2.0.18 starting
	2024-05-31T09:08:01: Config loaded from /etc/mosquitto/mosquitto.conf.
	2024-05-31T09:08:01: Opening websockets listen socket on port 8083.
	2024-05-31T09:08:01: Opening ipv6 listen socket on port 8883.
	2024-05-31T09:08:01: mosquitto version 2.0.18 running
	2024-05-31T09:08:01: New connection from fe80::742e:db1d:d1b2:0:63509 on port 8883.
	2024-05-31T09:08:03: New client connected from fe80::742e:db1d:d1b2:0:63509 as auto-85FC4C78-1503-FC9E-39E8-B878E6DB94AC (p2, c1, k60).
	2024-05-31T09:23:05: Client auto-85FC4C78-1503-FC9E-39E8-B878E6DB94AC disconnected: Protocol error.
	2024-05-31T09:23:23: New connection from fe80::742e:db1d:d1b2:0:63510 on port 8883.
	2024-05-31T09:23:24: New client connected from fe80::742e:db1d:d1b2:0:63510 as auto-1EB8D368-5B81-CC22-FF39-9666354EA49A (p2, c1, k60).
	2024-05-31T09:25:33: Client auto-1EB8D368-5B81-CC22-FF39-9666354EA49A disconnected: Protocol error.
	2024-05-31T09:25:36: New connection from fe80::742e:db1d:d1b2:0:63511 on port 8883.
	2024-05-31T09:25:38: New client connected from fe80::742e:db1d:d1b2:0:63511 as auto-A347E158-330E-B9CA-4771-A2DA1CF3FC19 (p2, c1, k60).
	2024-05-31T09:43:09: Client auto-A347E158-330E-B9CA-4771-A2DA1CF3FC19 disconnected: Protocol error.
	2024-05-31T09:43:13: New connection from fe80::742e:db1d:d1b2:0:63512 on port 8883.
	2024-05-31T09:43:14: New client connected from fe80::742e:db1d:d1b2:0:63512 as auto-DF2261FA-747D-2BF6-AEF5-357DE3F4A4E4 (p2, c1, k60).
	2024-05-31T09:50:01: Client auto-DF2261FA-747D-2BF6-AEF5-357DE3F4A4E4 disconnected: Protocol error.
	2024-05-31T09:50:01: New connection from fe80::742e:db1d:d1b2:0:63513 on port 8883.
	2024-05-31T09:50:03: New client connected from fe80::742e:db1d:d1b2:0:63513 as auto-7A5E6B0B-F673-F2F9-CEEA-4BD51C2CFE45 (p2, c1, k60).
	`
	shellhelper.WriteFile(mosquittoLogFile, mosquittoLog)
	time.Sleep(time.Millisecond * 2000)

	clients := ClientsProvider.GetClients()
	assert.Assert(t, len(clients) == 1)
}

func TestRestart2(t *testing.T) {

	mosquittoLog := `
	2024-05-31T08:30:39: New client connected from fe80::742e:db1d:d1b2:0:63505 as auto-8B3FBB5B-78FB-E14E-AB52-F5F629E0399F (p2, c1, k60).
	2024-05-31T08:46:22: Client auto-8B3FBB5B-78FB-E14E-AB52-F5F629E0399F disconnected: Protocol error.
	2024-05-31T08:46:24: New connection from fe80::742e:db1d:d1b2:0:63506 on port 8883.
	2024-05-31T08:46:27: New client connected from fe80::742e:db1d:d1b2:0:63506 as auto-6C16948F-A012-4FD3-5CA2-F81B577C3626 (p2, c1, k60).
	2024-05-31T08:55:00: Client auto-6C16948F-A012-4FD3-5CA2-F81B577C3626 disconnected: Protocol error.
	2024-05-31T08:55:11: New connection from fe80::742e:db1d:d1b2:0:63507 on port 8883.
	2024-05-31T08:55:14: New client connected from fe80::742e:db1d:d1b2:0:63507 as auto-351ABCD3-36F4-3F74-C522-3BBB8E94A855 (p2, c1, k60).
	2024-05-31T09:00:51: Client auto-351ABCD3-36F4-3F74-C522-3BBB8E94A855 disconnected: Protocol error.
	2024-05-31T09:00:54: New connection from fe80::742e:db1d:d1b2:0:63508 on port 8883.
	2024-05-31T09:00:58: New client connected from fe80::742e:db1d:d1b2:0:63508 as auto-B3215F63-12CD-1469-6CD8-B95651BF8270 (p2, c1, k60).
	2024-05-31T09:07:59: mosquitto version 2.0.18 terminating
	2024-05-31T09:08:01: mosquitto version 2.0.18 starting
	2024-05-31T09:08:01: Config loaded from /etc/mosquitto/mosquitto.conf.
	2024-05-31T09:08:01: Opening websockets listen socket on port 8083.
	2024-05-31T09:08:01: Opening ipv6 listen socket on port 8883.
	2024-05-31T09:08:01: mosquitto version 2.0.18 running
	2024-05-31T10:39:45: New connection from fe80::742e:db1d:d1b2:0:63517 on port 8883.
	2024-05-31T10:39:48: New client connected from fe80::742e:db1d:d1b2:0:63517 as auto-E53DEBCB-FA19-0A4F-D79B-E5E70B96F965 (p2, c1, k60).
	2024-05-31T10:52:56: Client auto-E53DEBCB-FA19-0A4F-D79B-E5E70B96F965 disconnected: Protocol error.
	2024-05-31T10:53:06: New connection from fe80::742e:db1d:d1b2:0:63518 on port 8883.
	2024-05-31T10:53:11: New client connected from fe80::742e:db1d:d1b2:0:63518 as auto-97F73EC3-9C37-D28B-191B-88C982DDB603 (p2, c1, k60).
	2024-05-31T10:54:02: Client auto-97F73EC3-9C37-D28B-191B-88C982DDB603 disconnected: Protocol error.
	2024-05-31T10:54:02: New connection from fe80::742e:db1d:d1b2:0:63519 on port 8883.
	2024-05-31T10:54:04: New client connected from fe80::742e:db1d:d1b2:0:63519 as auto-6C62BDAE-9999-B319-45E0-AE828EFE4BED (p2, c1, k60).
	2024-05-31T10:55:02: Client auto-6C62BDAE-9999-B319-45E0-AE828EFE4BED disconnected: Protocol error.
	2024-05-31T10:55:04: New connection from fe80::742e:db1d:d1b2:0:63520 on port 8883.
	2024-05-31T10:55:05: New client connected from fe80::742e:db1d:d1b2:0:63520 as auto-3DC692E8-3010-8E3A-C296-C6EB738E1CB0 (p2, c1, k60).
	2024-05-31T10:57:26: Client auto-3DC692E8-3010-8E3A-C296-C6EB738E1CB0 disconnected: Protocol error.
	2024-05-31T10:57:26: New connection from fe80::742e:db1d:d1b2:0:63521 on port 8883.
	2024-05-31T10:57:28: New client connected from fe80::742e:db1d:d1b2:0:63521 as auto-39293EC6-45A0-9033-ACD8-228631C268C3 (p2, c1, k60).
	2024-05-31T11:02:48: Client auto-39293EC6-45A0-9033-ACD8-228631C268C3 disconnected: Protocol error.
	2024-05-31T11:02:48: New connection from fe80::742e:db1d:d1b2:0:63522 on port 8883.
	2024-05-31T11:02:50: New client connected from fe80::742e:db1d:d1b2:0:63522 as auto-400342B1-D010-F8BE-391A-25C1F335B9C1 (p2, c1, k60).
	2024-05-31T11:05:12: Client auto-400342B1-D010-F8BE-391A-25C1F335B9C1 disconnected: Protocol error.
	2024-05-31T11:05:12: New connection from fe80::742e:db1d:d1b2:0:63523 on port 8883.
	2024-05-31T11:05:13: New client connected from fe80::742e:db1d:d1b2:0:63523 as auto-2ECD4995-FFA4-0A5E-E3AB-1A2ACDD788A7 (p2, c1, k60).
	2024-05-31T11:07:24: Client auto-2ECD4995-FFA4-0A5E-E3AB-1A2ACDD788A7 disconnected: Protocol error.
	2024-05-31T11:07:30: New connection from fe80::742e:db1d:d1b2:0:63524 on port 8883.
	2024-05-31T11:07:32: New client connected from fe80::742e:db1d:d1b2:0:63524 as auto-2AEE860E-4690-AF35-E983-93DA0C69FB31 (p2, c1, k60).
	2024-05-31T11:09:37: Client auto-2AEE860E-4690-AF35-E983-93DA0C69FB31 disconnected: Protocol error.
	2024-05-31T11:09:39: New connection from fe80::742e:db1d:d1b2:0:63525 on port 8883.
	2024-05-31T11:09:41: New client connected from fe80::742e:db1d:d1b2:0:63525 as auto-E9C09C82-07D9-DB57-02C9-A47BBBBA9752 (p2, c1, k60).
	2024-05-31T11:17:40: Client auto-E9C09C82-07D9-DB57-02C9-A47BBBBA9752 disconnected: Protocol error.
	2024-05-31T11:17:40: New connection from fe80::742e:db1d:d1b2:0:63526 on port 8883.
	2024-05-31T11:17:42: New client connected from fe80::742e:db1d:d1b2:0:63526 as auto-CC4D6A72-64A0-F5D5-4147-EBB6AD55C3F5 (p2, c1, k60).
	2024-05-31T11:18:31: Client auto-CC4D6A72-64A0-F5D5-4147-EBB6AD55C3F5 disconnected: Protocol error.
	2024-05-31T11:41:17: New connection from fe80::742e:db1d:d1b2:0:63530 on port 8883.
	2024-05-31T11:41:19: New client connected from fe80::742e:db1d:d1b2:0:63530 as auto-31636A44-2C9E-29FE-75F8-574E99CE37D2 (p2, c1, k60).
	2024-05-31T11:51:49: Client auto-31636A44-2C9E-29FE-75F8-574E99CE37D2 has exceeded timeout, disconnecting.
	2024-05-31T09:07:59: mosquitto version 2.0.18 terminating
	2024-05-31T09:08:01: mosquitto version 2.0.18 starting
	2024-05-31T09:08:01: Config loaded from /etc/mosquitto/mosquitto.conf.
	2024-05-31T09:08:01: Opening websockets listen socket on port 8083.
	2024-05-31T09:08:01: Opening ipv6 listen socket on port 8883.
	2024-05-31T09:08:01: mosquitto version 2.0.18 running	
	2024-05-31T13:19:55: New connection from fe80::742e:db06:5dec:0:49403 on port 8883.
	2024-05-31T13:19:56: New client connected from fe80::742e:db06:5dec:0:49403 as shoe/mqtt24test/somedata (p2, c1, k20).
	2024-05-31T13:19:57: shoe/mqtt24test/somedata 0 shoe/mqtt24test/somedata/uart_send
	2024-05-31T13:19:57: shoe/mqtt24test/somedata 0 shoe/mqtt24test/somedata/i2c_send
	2024-05-31T13:20:37: New connection from fe80::742e:db06:5dec:0:49404 on port 8883.
	2024-05-31T13:20:39: New client connected from fe80::742e:db06:5dec:0:49404 as shoe/mqtt24test/shoesMetric (p2, c1, k20).
	2024-05-31T13:20:39: shoe/mqtt24test/shoesMetric 0 shoe/mqtt24test/shoesMetric/uart_send
	2024-05-31T13:20:39: shoe/mqtt24test/shoesMetric 0 shoe/mqtt24test/shoesMetric/i2c_send
	2024-05-31T13:20:55: Client shoe/mqtt24test/somedata has exceeded timeout, disconnecting.
	2024-05-31T13:21:06: New connection from fe80::742e:db06:5dec:0:49403 on port 8883.
	2024-05-31T13:21:08: New client connected from fe80::742e:db06:5dec:0:49403 as shoe/mqtt24test/timetoWorldDomination (p2, c1, k20).
	2024-05-31T13:21:08: shoe/mqtt24test/timetoWorldDomination 0 shoe/mqtt24test/timetoWorldDomination/uart_send
	2024-05-31T13:21:08: shoe/mqtt24test/timetoWorldDomination 0 shoe/mqtt24test/timetoWorldDomination/i2c_send
	2024-05-31T13:21:31: Client shoe/mqtt24test/shoesMetric has exceeded timeout, disconnecting.
	2024-05-31T13:21:42: Client shoe/mqtt24test/timetoWorldDomination disconnected: Protocol error.
	2024-05-31T13:21:42: New connection from fe80::742e:db06:5dec:0:49404 on port 8883.
	2024-05-31T13:21:44: New client connected from fe80::742e:db06:5dec:0:49404 as shoe/mqtt24test/timetoWorldDomination (p2, c1, k20).
	2024-05-31T13:21:44: shoe/mqtt24test/timetoWorldDomination 0 shoe/mqtt24test/timetoWorldDomination/uart_send
	2024-05-31T13:21:44: shoe/mqtt24test/timetoWorldDomination 0 shoe/mqtt24test/timetoWorldDomination/i2c_send
	`
	shellhelper.WriteFile(mosquittoLogFile, mosquittoLog)
	time.Sleep(time.Millisecond * 2000)

	clients := ClientsProvider.GetClients()

	log.Default().Printf("Length: clients %d\n", len(clients))
	log.Default().Print(clients)

	assert.Assert(t, len(clients) == 1)
	assert.Assert(t, clients[0].Id == "shoe/mqtt24test/timetoWorldDomination")
	assert.Assert(t, clients[0].Ip == "fe80::742e:db06:5dec:0")
}
func TestRestart3(t *testing.T) {

	mosquittoLog := `
	2024-05-31T09:08:01: mosquitto version 2.0.18 starting
	2024-05-31T09:08:01: Config loaded from /etc/mosquitto/mosquitto.conf.
	2024-05-31T09:08:01: Opening websockets listen socket on port 8083.
	2024-05-31T09:08:01: Opening ipv6 listen socket on port 8883.
	2024-05-31T09:08:01: mosquitto version 2.0.18 running
	2024-05-31T10:39:45: New connection from fe80::742e:db1d:d1b2:0:63517 on port 8883.
	2024-05-31T10:39:48: New client connected from fe80::742e:db1d:d1b2:0:63517 as auto-E53DEBCB-FA19-0A4F-D79B-E5E70B96F965 (p2, c1, k60).
	2024-05-31T10:52:56: Client auto-E53DEBCB-FA19-0A4F-D79B-E5E70B96F965 disconnected: Protocol error.
	2024-05-31T10:53:06: New connection from fe80::742e:db1d:d1b2:0:63518 on port 8883.
	2024-05-31T10:53:11: New client connected from fe80::742e:db1d:d1b2:0:63518 as auto-97F73EC3-9C37-D28B-191B-88C982DDB603 (p2, c1, k60).
	2024-05-31T10:54:02: Client auto-97F73EC3-9C37-D28B-191B-88C982DDB603 disconnected: Protocol error.
	2024-05-31T10:54:02: New connection from fe80::742e:db1d:d1b2:0:63519 on port 8883.
	2024-05-31T10:54:04: New client connected from fe80::742e:db1d:d1b2:0:63519 as auto-6C62BDAE-9999-B319-45E0-AE828EFE4BED (p2, c1, k60).
	2024-05-31T10:55:02: Client auto-6C62BDAE-9999-B319-45E0-AE828EFE4BED disconnected: Protocol error.
	2024-05-31T10:55:04: New connection from fe80::742e:db1d:d1b2:0:63520 on port 8883.
	2024-05-31T10:55:05: New client connected from fe80::742e:db1d:d1b2:0:63520 as auto-3DC692E8-3010-8E3A-C296-C6EB738E1CB0 (p2, c1, k60).
	2024-05-31T10:57:26: Client auto-3DC692E8-3010-8E3A-C296-C6EB738E1CB0 disconnected: Protocol error.
	2024-05-31T10:57:26: New connection from fe80::742e:db1d:d1b2:0:63521 on port 8883.
	2024-05-31T10:57:28: New client connected from fe80::742e:db1d:d1b2:0:63521 as auto-39293EC6-45A0-9033-ACD8-228631C268C3 (p2, c1, k60).
	2024-05-31T11:02:48: Client auto-39293EC6-45A0-9033-ACD8-228631C268C3 disconnected: Protocol error.
	2024-05-31T11:02:48: New connection from fe80::742e:db1d:d1b2:0:63522 on port 8883.
	2024-05-31T11:02:50: New client connected from fe80::742e:db1d:d1b2:0:63522 as auto-400342B1-D010-F8BE-391A-25C1F335B9C1 (p2, c1, k60).
	2024-05-31T11:05:12: Client auto-400342B1-D010-F8BE-391A-25C1F335B9C1 disconnected: Protocol error.
	2024-05-31T11:05:12: New connection from fe80::742e:db1d:d1b2:0:63523 on port 8883.
	2024-05-31T11:05:13: New client connected from fe80::742e:db1d:d1b2:0:63523 as auto-2ECD4995-FFA4-0A5E-E3AB-1A2ACDD788A7 (p2, c1, k60).
	2024-05-31T11:07:24: Client auto-2ECD4995-FFA4-0A5E-E3AB-1A2ACDD788A7 disconnected: Protocol error.
	2024-05-31T11:07:30: New connection from fe80::742e:db1d:d1b2:0:63524 on port 8883.
	2024-05-31T11:07:32: New client connected from fe80::742e:db1d:d1b2:0:63524 as auto-2AEE860E-4690-AF35-E983-93DA0C69FB31 (p2, c1, k60).
	2024-05-31T11:09:37: Client auto-2AEE860E-4690-AF35-E983-93DA0C69FB31 disconnected: Protocol error.
	2024-05-31T11:09:39: New connection from fe80::742e:db1d:d1b2:0:63525 on port 8883.
	2024-05-31T11:09:41: New client connected from fe80::742e:db1d:d1b2:0:63525 as auto-E9C09C82-07D9-DB57-02C9-A47BBBBA9752 (p2, c1, k60).
	2024-05-31T11:17:40: Client auto-E9C09C82-07D9-DB57-02C9-A47BBBBA9752 disconnected: Protocol error.
	2024-05-31T11:17:40: New connection from fe80::742e:db1d:d1b2:0:63526 on port 8883.
	2024-05-31T11:17:42: New client connected from fe80::742e:db1d:d1b2:0:63526 as auto-CC4D6A72-64A0-F5D5-4147-EBB6AD55C3F5 (p2, c1, k60).
	2024-05-31T11:18:31: Client auto-CC4D6A72-64A0-F5D5-4147-EBB6AD55C3F5 disconnected: Protocol error.
	2024-05-31T11:41:17: New connection from fe80::742e:db1d:d1b2:0:63530 on port 8883.
	2024-05-31T11:41:19: New client connected from fe80::742e:db1d:d1b2:0:63530 as auto-31636A44-2C9E-29FE-75F8-574E99CE37D2 (p2, c1, k60).
	2024-05-31T11:51:49: Client auto-31636A44-2C9E-29FE-75F8-574E99CE37D2 has exceeded timeout, disconnecting.
	`
	shellhelper.WriteFile(mosquittoLogFile, mosquittoLog)
	time.Sleep(time.Millisecond * 2000)

	clients := ClientsProvider.GetClients()

	log.Default().Printf("Length: clients %d\n", len(clients))
	log.Default().Print(clients)

	assert.Assert(t, len(clients) == 0)
}

func TestRestart5(t *testing.T) {

	mosquittoLog := `
	2024-05-31T08:30:39: New client connected from fe80::742e:db1d:d1b2:0:63505 as auto-8B3FBB5B-78FB-E14E-AB52-F5F629E0399F (p2, c1, k60).
	2024-05-31T08:46:22: Client auto-8B3FBB5B-78FB-E14E-AB52-F5F629E0399F disconnected: Protocol error.
	2024-05-31T08:46:24: New connection from fe80::742e:db1d:d1b2:0:63506 on port 8883.
	2024-05-31T08:46:27: New client connected from fe80::742e:db1d:d1b2:0:63506 as auto-6C16948F-A012-4FD3-5CA2-F81B577C3626 (p2, c1, k60).
	2024-05-31T08:55:00: Client auto-6C16948F-A012-4FD3-5CA2-F81B577C3626 disconnected: Protocol error.
	2024-05-31T08:55:11: New connection from fe80::742e:db1d:d1b2:0:63507 on port 8883.
	2024-05-31T08:55:14: New client connected from fe80::742e:db1d:d1b2:0:63507 as auto-351ABCD3-36F4-3F74-C522-3BBB8E94A855 (p2, c1, k60).
	2024-05-31T09:00:51: Client auto-351ABCD3-36F4-3F74-C522-3BBB8E94A855 disconnected: Protocol error.
	2024-05-31T09:00:54: New connection from fe80::742e:db1d:d1b2:0:63508 on port 8883.
	2024-05-31T09:00:58: New client connected from fe80::742e:db1d:d1b2:0:63508 as auto-B3215F63-12CD-1469-6CD8-B95651BF8270 (p2, c1, k60).
	2024-05-31T09:07:59: mosquitto version 2.0.18 terminating
	2024-05-31T09:08:01: mosquitto version 2.0.18 starting
	2024-05-31T09:08:01: Config loaded from /etc/mosquitto/mosquitto.conf.
	2024-05-31T09:08:01: Opening websockets listen socket on port 8083.
	2024-05-31T09:08:01: Opening ipv6 listen socket on port 8883.
	2024-05-31T09:08:01: mosquitto version 2.0.18 running
	2024-05-31T10:39:45: New connection from fe80::742e:db1d:d1b2:0:63517 on port 8883.
	2024-05-31T10:39:48: New client connected from fe80::742e:db1d:d1b2:0:63517 as auto-E53DEBCB-FA19-0A4F-D79B-E5E70B96F965 (p2, c1, k60).
	2024-05-31T10:52:56: Client auto-E53DEBCB-FA19-0A4F-D79B-E5E70B96F965 disconnected: Protocol error.
	2024-05-31T10:53:06: New connection from fe80::742e:db1d:d1b2:0:63518 on port 8883.
	2024-05-31T10:53:11: New client connected from fe80::742e:db1d:d1b2:0:63518 as auto-97F73EC3-9C37-D28B-191B-88C982DDB603 (p2, c1, k60).
	2024-05-31T10:54:02: Client auto-97F73EC3-9C37-D28B-191B-88C982DDB603 disconnected: Protocol error.
	2024-05-31T10:54:02: New connection from fe80::742e:db1d:d1b2:0:63519 on port 8883.
	2024-05-31T10:54:04: New client connected from fe80::742e:db1d:d1b2:0:63519 as auto-6C62BDAE-9999-B319-45E0-AE828EFE4BED (p2, c1, k60).
	2024-05-31T10:55:02: Client auto-6C62BDAE-9999-B319-45E0-AE828EFE4BED disconnected: Protocol error.
	2024-05-31T10:55:04: New connection from fe80::742e:db1d:d1b2:0:63520 on port 8883.
	2024-05-31T10:55:05: New client connected from fe80::742e:db1d:d1b2:0:63520 as auto-3DC692E8-3010-8E3A-C296-C6EB738E1CB0 (p2, c1, k60).
	2024-05-31T10:57:26: Client auto-3DC692E8-3010-8E3A-C296-C6EB738E1CB0 disconnected: Protocol error.
	2024-05-31T10:57:26: New connection from fe80::742e:db1d:d1b2:0:63521 on port 8883.
	2024-05-31T10:57:28: New client connected from fe80::742e:db1d:d1b2:0:63521 as auto-39293EC6-45A0-9033-ACD8-228631C268C3 (p2, c1, k60).
	2024-05-31T11:02:48: Client auto-39293EC6-45A0-9033-ACD8-228631C268C3 disconnected: Protocol error.
	2024-05-31T11:02:48: New connection from fe80::742e:db1d:d1b2:0:63522 on port 8883.
	2024-05-31T11:02:50: New client connected from fe80::742e:db1d:d1b2:0:63522 as auto-400342B1-D010-F8BE-391A-25C1F335B9C1 (p2, c1, k60).
	2024-05-31T11:05:12: Client auto-400342B1-D010-F8BE-391A-25C1F335B9C1 disconnected: Protocol error.
	2024-05-31T11:05:12: New connection from fe80::742e:db1d:d1b2:0:63523 on port 8883.
	2024-05-31T11:05:13: New client connected from fe80::742e:db1d:d1b2:0:63523 as auto-2ECD4995-FFA4-0A5E-E3AB-1A2ACDD788A7 (p2, c1, k60).
	2024-05-31T11:07:24: Client auto-2ECD4995-FFA4-0A5E-E3AB-1A2ACDD788A7 disconnected: Protocol error.
	2024-05-31T11:07:30: New connection from fe80::742e:db1d:d1b2:0:63524 on port 8883.
	2024-05-31T11:07:32: New client connected from fe80::742e:db1d:d1b2:0:63524 as auto-2AEE860E-4690-AF35-E983-93DA0C69FB31 (p2, c1, k60).
	2024-05-31T11:09:37: Client auto-2AEE860E-4690-AF35-E983-93DA0C69FB31 disconnected: Protocol error.
	2024-05-31T11:09:39: New connection from fe80::742e:db1d:d1b2:0:63525 on port 8883.
	2024-05-31T11:09:41: New client connected from fe80::742e:db1d:d1b2:0:63525 as auto-E9C09C82-07D9-DB57-02C9-A47BBBBA9752 (p2, c1, k60).
	2024-05-31T11:17:40: Client auto-E9C09C82-07D9-DB57-02C9-A47BBBBA9752 disconnected: Protocol error.
	2024-05-31T11:17:40: New connection from fe80::742e:db1d:d1b2:0:63526 on port 8883.
	2024-05-31T11:17:42: New client connected from fe80::742e:db1d:d1b2:0:63526 as auto-CC4D6A72-64A0-F5D5-4147-EBB6AD55C3F5 (p2, c1, k60).
	2024-05-31T11:18:31: Client auto-CC4D6A72-64A0-F5D5-4147-EBB6AD55C3F5 disconnected: Protocol error.
	2024-05-31T11:41:17: New connection from fe80::742e:db1d:d1b2:0:63530 on port 8883.
	2024-05-31T11:41:19: New client connected from fe80::742e:db1d:d1b2:0:63530 as auto-31636A44-2C9E-29FE-75F8-574E99CE37D2 (p2, c1, k60).
	2024-05-31T11:51:49: Client auto-31636A44-2C9E-29FE-75F8-574E99CE37D2 has exceeded timeout, disconnecting.

	2024-05-31T13:19:55: New connection from fe80::742e:db06:5dec:0:49403 on port 8883.
	2024-05-31T13:19:56: New client connected from fe80::742e:db06:5dec:0:49403 as shoe/mqtt24test/somedata (p2, c1, k20).
	2024-05-31T13:19:57: shoe/mqtt24test/somedata 0 shoe/mqtt24test/somedata/uart_send
	2024-05-31T13:19:57: shoe/mqtt24test/somedata 0 shoe/mqtt24test/somedata/i2c_send
	2024-05-31T13:20:37: New connection from fe80::742e:db06:5dec:0:49404 on port 8883.
	2024-05-31T13:20:39: New client connected from fe80::742e:db06:5dec:0:49404 as shoe/mqtt24test/shoesMetric (p2, c1, k20).
	2024-05-31T13:20:39: shoe/mqtt24test/shoesMetric 0 shoe/mqtt24test/shoesMetric/uart_send
	2024-05-31T13:20:39: shoe/mqtt24test/shoesMetric 0 shoe/mqtt24test/shoesMetric/i2c_send
	2024-05-31T13:20:55: Client shoe/mqtt24test/somedata has exceeded timeout, disconnecting.
	2024-05-31T13:21:06: New connection from fe80::742e:db06:5dec:0:49403 on port 8883.
	2024-05-31T13:21:08: New client connected from fe80::742e:db06:5dec:0:49403 as shoe/mqtt24test/timetoWorldDomination (p2, c1, k20).
	2024-05-31T13:21:08: shoe/mqtt24test/timetoWorldDomination 0 shoe/mqtt24test/timetoWorldDomination/uart_send
	2024-05-31T13:21:08: shoe/mqtt24test/timetoWorldDomination 0 shoe/mqtt24test/timetoWorldDomination/i2c_send
	2024-05-31T13:21:31: Client shoe/mqtt24test/shoesMetric has exceeded timeout, disconnecting.
	2024-05-31T13:21:42: Client shoe/mqtt24test/timetoWorldDomination disconnected: Protocol error.
	2024-05-31T13:21:42: New connection from fe80::742e:db06:5dec:0:49404 on port 8883.
	2024-05-31T13:21:44: New client connected from fe80::742e:db06:5dec:0:49404 as shoe/mqtt24test/timetoWorldDomination (p2, c1, k20).
	2024-05-31T13:21:44: shoe/mqtt24test/timetoWorldDomination 0 shoe/mqtt24test/timetoWorldDomination/uart_send
	2024-05-31T13:21:44: shoe/mqtt24test/timetoWorldDomination 0 shoe/mqtt24test/timetoWorldDomination/i2c_send
	`
	shellhelper.WriteFile(mosquittoLogFile, mosquittoLog)
	time.Sleep(time.Millisecond * 1000)

	clients := ClientsProvider.GetClients()

	log.Default().Printf("Length: clients %d\n", len(clients))
	log.Default().Print(clients)

	assert.Assert(t, len(clients) == 1)
	assert.Assert(t, clients[0].Id == "shoe/mqtt24test/timetoWorldDomination")
	assert.Assert(t, clients[0].Ip == "fe80::742e:db06:5dec:0")
}
