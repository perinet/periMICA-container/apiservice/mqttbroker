/*
 * Copyright (c) Perinet GmbH
 * All rights reserved
 *
 * This software is dual-licensed: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 as
 * published by the Free Software Foundation. For the terms of this
 * license, see http://www.fsf.org/licensing/licenses/agpl-3.0.html,
 * or contact us at https://server.io/contact/ when you want license
 * this software under a commercial license.
 */

// Package apiservice_diagnostics_mosquitto implements the RESTful API to the
// mosquitto diagnostics service
package clients

import (
	"log"
	"strings"
	"sync"
	"time"

	"github.com/godbus/dbus/v5"
	"github.com/holoplot/go-avahi"
	"github.com/nxadm/tail"
)

type Client struct {
	Hostname  string `json:"hostname"`
	Id        string `json:"id"`
	Ip        string `json:"ip"`
	Timestamp string `json:"timestamp"`
}

type Config struct {
	LogFilePath string
}

var (
	logger          log.Logger        = *log.Default()
	clients         map[string]Client = make(map[string]Client)
	clients_lock    sync.Mutex
	channel         = make(chan string)
	logfilepath     = "/var/log/mosquitto.log"
	readerStarted   = false
	dbus_connection *dbus.Conn
	avahi_server    *avahi.Server
)

func init() {
	logger.SetPrefix("mosquitto/clients: ")
	logger.Print("starting")

	var err error
	dbus_connection, err = dbus.SystemBus()
	if err != nil {
		logger.Printf("Cannot get system bus: %v", err)
	} else {
		avahi_server, err = avahi.ServerNew(dbus_connection)
		if err != nil {
			logger.Printf("Avahi new failed: %v", err)
		}
	}
	go parse_mosquitto_log(channel)
}

func SetConfig(config Config) {
	logfilepath = config.LogFilePath
	if !readerStarted {
		go fileReader(channel)
		readerStarted = true
	}
}

func fileReader(ch chan string) {
	for {
		// Create a tail
		t, err := tail.TailFile(
			logfilepath, tail.Config{Follow: true, ReOpen: true, MustExist: true})
		if err != nil {
			logger.Println("failed tailing logfile ", err)
			time.Sleep(time.Millisecond * 500)
			continue
		}

		// move text of each received line
		for line := range t.Lines {
			ch <- line.Text
		}
	}
}

func broker_start_detect(log string) bool {
	const prefix = "mosquitto version"
	const suffix = "running"
	if strings.HasPrefix(log, prefix) && strings.HasSuffix(log, suffix) {
		return true
	}
	return false
}

func add_client(log string, timestamp string) bool {
	const prefix = "New client connected from "
	if strings.HasPrefix(log, prefix) {
		// determine client IP
		tmp := strings.Split(strings.TrimPrefix(log, prefix), " ")

		// remove port
		idx := strings.LastIndex(tmp[0], ":")
		// logger.Printf("index of last ':': %d\n", idx)

		var cl Client
		cl.Ip = tmp[0][:idx]
		// logger.Printf("ip: %s\n", cl.Ip)

		cl.Hostname = cl.Ip
		cl.Id = tmp[2]
		cl.Timestamp = timestamp
		logger.Printf("Adding Client (id=%s, ip=%s).", cl.Id, cl.Ip)
		clients_lock.Lock()
		clients[cl.Id] = cl
		clients_lock.Unlock()

		go func(id string, ip string) {
			// resolve host name by address
			if avahi_server != nil {
				res, err := avahi_server.ResolveAddress(avahi.InterfaceUnspec, avahi.ProtoInet6, ip, 0)
				if err != nil {
					logger.Printf("Unable to resolve hostname of address %s", ip)
				} else {
					clients_lock.Lock()
					cl, found := clients[id]
					if found {
						cl.Hostname = res.Name
						clients[id] = cl
					}
					clients_lock.Unlock()
				}
			} else {
				logger.Printf("No connection to avahi while resolving hostname of address %s", ip)
			}
		}(cl.Id, cl.Ip)

		return true
	}
	return false
}

func remove_client(log string) bool {
	const prefix = "Client "
	var suffix = [...]string{
		" closed its connection.",
		" disconnected.",
		" disconnected, not authorised.",
		" disconnected: Protocol error.",
		" has exceeded timeout, disconnecting.",
		" already connected, closing old connection.",
	}

	if strings.HasPrefix(log, prefix) {
		for _, entry := range suffix {
			if strings.HasSuffix(log, entry) {
				// determine id
				id := strings.TrimPrefix(strings.TrimSuffix(log, entry), prefix)
				logger.Printf("Removing Client (id=%s).", id)
				clients_lock.Lock()
				delete(clients, id)
				clients_lock.Unlock()
			}
		}
	}
	return false
}

func parse_mosquitto_log(ch chan string) {
	for {
		var rec = <-ch
		rec = strings.TrimSpace(rec)
		// split timestamp/log
		i := strings.Index(rec, " ")
		if i < 0 {
			continue
		}
		timestamp := strings.TrimSuffix(rec[:i], ":")
		log := rec[i+1:]

		res := broker_start_detect(log)
		if res {
			logger.Printf("Broker restarted (%s).", timestamp)

			// clear clients map
			clients_lock.Lock()
			clients = make(map[string]Client)
			clients_lock.Unlock()

			continue
		}
		res = add_client(log, timestamp)
		if res {
			continue
		}
		_ = remove_client(log)
	}
}

func GetClients() []Client {
	clients_lock.Lock()
	clients_list := make([]Client, 0, len(clients))
	for _, entry := range clients {
		clients_list = append(clients_list, entry)
	}
	clients_lock.Unlock()
	return clients_list
}
