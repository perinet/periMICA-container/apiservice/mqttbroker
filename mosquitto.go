/*
Copyright (c) Perinet GmbH
All rights reserved

This software is dual-licensed: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License version 3 as
published by the Free Software Foundation. For the terms of this
license, see http://www.fsf.org/licensing/licenses/agpl-3.0.html,
or contact us at https://perinet.io/contact/ when you want license
this software under a commercial license.
*/

package mosquitto

import (
	"encoding/json"
	"log"
	"net/http"
	"os"
	"strings"

	"gitlab.com/perinet/generic/apiservice/dnssd"
	"gitlab.com/perinet/generic/lib/httpserver"
	"gitlab.com/perinet/generic/lib/httpserver/auth"
	"gitlab.com/perinet/generic/lib/httpserver/periHttp"
	"gitlab.com/perinet/generic/lib/httpserver/rbac"
	"gitlab.com/perinet/generic/lib/utils/intHttp"
	"gitlab.com/perinet/generic/lib/utils/shellhelper"
	"gitlab.com/perinet/periMICA-container/apiservice/node"
	"gitlab.com/perinet/periMICA-container/apiservice/node/nodeErrorStatus"
	"gitlab.com/perinet/periMICA-container/apiservice/security"

	ClientsProvider "gitlab.com/perinet/periMICA-container/apiservice/mosquitto/clients"
)

func PathsGet() []httpserver.PathInfo {
	return []httpserver.PathInfo{
		{Url: "/mqttbroker", Method: httpserver.GET, Role: rbac.USER, Call: Mqttbroker_Get},
		{Url: "/mqttbroker/mosquitto/advanced-config", Method: httpserver.GET, Role: rbac.ADMIN, Call: Mqttbroker_Config_Get},
		{Url: "/mqttbroker/mosquitto/advanced-config", Method: httpserver.PATCH, Role: rbac.ADMIN, Call: Mqttbroker_Config_Set},
		{Url: "/mqttbroker/clients", Method: httpserver.GET, Role: rbac.USER, Call: Mqttbroker_Clients_Get},
		{Url: "/mqttbroker/log", Method: httpserver.GET, Role: rbac.ADMIN, Call: Mqttbroker_Log_Get},
		{Url: "/mqttbroker/restart", Method: httpserver.PATCH, Role: rbac.ADMIN, Call: MQTTBroker_Restart},
	}
}

const (
	config_gen_delimiter                  string = "# automatic generated from here"
	mosquitto_config_section_template_TLS string = `
allow_anonymous true
certfile <certfile_path>
keyfile <keyfile_path>
#cafile <cafile_path>
require_certificate false
use_identity_as_username false
`
	mosquitto_config_section_template_mTLS = `
allow_anonymous false
certfile <certfile_path>
keyfile <keyfile_path>
cafile <cafile_path>
require_certificate true
use_identity_as_username true
`
)

type ServiceConfig struct {
	StoragePath             string
	ListenerConfigPath      string
	MosquittoLogFilePath    string
	MosquittoUserConfigPath string
	BrokerRestartCallback   func() bool
}

var (
	hostCertFile = "/server.pem"
	hostKeyFile  = "/server.key"
	rootCertFile = "/root-ca.pem"

	serviceConfig  ServiceConfig
	logger         log.Logger = *log.Default()
	mqttBrokerInfo            = MQTTBrokerInfo{ApiVersion: 21, ErrorStatus: nodeErrorStatus.NO_ERROR}
)

type MQTTBrokerInfo struct {
	ApiVersion    uint32               `json:"api_version"`
	ErrorStatus   node.NodeErrorStatus `json:"error_status"`
	SecurityLevel string               `json:"security_level"`
}

func init() {
	logger.SetPrefix("Mosquitto(MQTTBroker) ApiService: ")
	logger.Println("initializing...")
	//set default config
	serviceConfig = ServiceConfig{
		StoragePath:             "/var/lib/apiservice-mosquitto",
		ListenerConfigPath:      "/etc/mosquitto/conf.d/00_mqtt-listener.conf",
		MosquittoUserConfigPath: "/etc/mosquitto/conf.d/user.conf",
		MosquittoLogFilePath:    "/var/log/mosquitto.log",
		BrokerRestartCallback:   func() bool { logger.Println("WARNING: empty BrokerRestartCallback"); return false },
	}
	ClientsProvider.SetConfig(ClientsProvider.Config{LogFilePath: serviceConfig.MosquittoLogFilePath})
	node.RegisterNodeEventCallback(nodeInfoUpdated)
	security.RegisterSecurityEventCallback(securityConfigUpdated)
}

func SetServiceConfig(config ServiceConfig) {
	intHttp.Delete(node.ErrorStatusReset, nil)
	mqttBrokerInfo.ErrorStatus = nodeErrorStatus.NO_ERROR

	if config.StoragePath != "" {
		serviceConfig.StoragePath = config.StoragePath
	}
	if config.ListenerConfigPath != "" {
		serviceConfig.ListenerConfigPath = config.ListenerConfigPath
	}
	if config.MosquittoUserConfigPath != "" {
		serviceConfig.MosquittoUserConfigPath = config.MosquittoUserConfigPath
	}
	if config.MosquittoLogFilePath != "" {
		serviceConfig.MosquittoLogFilePath = config.MosquittoLogFilePath
	}
	ClientsProvider.SetConfig(ClientsProvider.Config{LogFilePath: serviceConfig.MosquittoLogFilePath})

	if config.BrokerRestartCallback != nil {
		serviceConfig.BrokerRestartCallback = config.BrokerRestartCallback
	}
	securityConfigUpdated()
	nodeInfoUpdated()
}

func changeNodeStatus(nodeErrorStatus node.NodeErrorStatus, reason ...string) {
	oldStatus := mqttBrokerInfo.ErrorStatus
	nodeErrorStatusJSON, _ := json.Marshal(nodeErrorStatus)
	intHttp.Put(node.ErrorStatusSet, nodeErrorStatusJSON, nil)
	logger.Println("Change NodeErrorStatus ", oldStatus.String(), " -> ", nodeErrorStatus.String(), " reason:", reason)
	mqttBrokerInfo.ErrorStatus = nodeErrorStatus
}

func updateMosquittoConfig(authEnabled bool) {
	content, _ := os.ReadFile(serviceConfig.ListenerConfigPath)

	mosqConfig, _, _ := strings.Cut(string(content), config_gen_delimiter)
	mosqConfig = strings.TrimSpace(mosqConfig)
	mosqConfig += "\n" + config_gen_delimiter + "\n"
	mosqConfigSuffix := ""

	if authEnabled {
		// mTLS config
		mosqConfigSuffix = mosquitto_config_section_template_mTLS
	} else {
		// TLS only config
		mosqConfigSuffix = mosquitto_config_section_template_TLS
	}
	mosqConfigSuffix = strings.Replace(mosqConfigSuffix, "<certfile_path>", serviceConfig.StoragePath+hostCertFile, 1)
	mosqConfigSuffix = strings.Replace(mosqConfigSuffix, "<keyfile_path>", serviceConfig.StoragePath+hostKeyFile, 1)
	mosqConfigSuffix = strings.Replace(mosqConfigSuffix, "<cafile_path>", serviceConfig.StoragePath+rootCertFile, 1)

	shellhelper.WriteFile(serviceConfig.ListenerConfigPath, mosqConfig+mosqConfigSuffix)
}

func writeCertificates() {
	caCert := intHttp.Get(security.Security_TrustAnchor_Get, map[string]string{"trust_anchor": "root_ca_cert"})
	hostCert := intHttp.Get(security.Security_Certificate_Get, map[string]string{"certificate": "host_cert"})
	hostCertKey := intHttp.Get(security.Security_Certificate_Key_Get, map[string]string{"certificate": "host_cert"})
	shellhelper.WriteFile(serviceConfig.StoragePath+rootCertFile, string(caCert))
	shellhelper.WriteFile(serviceConfig.StoragePath+hostCertFile, string(hostCert))
	shellhelper.WriteFile(serviceConfig.StoragePath+hostKeyFile, string(hostCertKey))
}

func setDnssdService() {
	serviceName := "_secure-mqtt._tcp"
	var servicePort uint16 = 8883

	var data []byte
	var err error
	data = intHttp.Get(node.NodeInfoGet, nil)
	var nodeInfo node.NodeInfo
	err = json.Unmarshal(data, &nodeInfo)
	if err != nil {
		logger.Println("Failed to fetch NodeInfo: ", err.Error())
	}

	data = intHttp.Get(node.ProductionInfoGet, nil)
	var productionInfo node.ProductionInfo
	err = json.Unmarshal(data, &productionInfo)
	if err != nil {
		logger.Println("Failed to fetch NodeInfo: ", err.Error())
	}

	//start advertising via dnssd
	dnssdServiceInfo := dnssd.DNSSDServiceInfo{ServiceName: serviceName, Port: servicePort}
	txt_record := dnssd.DNSSDTxtRecordHttps{Format: "uAPI"}
	txt_record.ApiVersion = "21"
	txt_record.ApplicationName = nodeInfo.Config.ApplicationName
	txt_record.ElementName = nodeInfo.Config.ElementName
	txt_record.ProductName = productionInfo.ProductName
	txt_record.ErrorStatus = nodeInfo.ErrorStatus
	dnssdServiceInfo.TxtRecord = txt_record
	data, _ = json.Marshal(dnssdServiceInfo)
	intHttp.Put(dnssd.DNSSDServiceInfoAdvertiseSet, data, map[string]string{"service_name": dnssdServiceInfo.ServiceName})
}

func nodeInfoUpdated() {
	setDnssdService()
}

func securityConfigUpdated() {
	data := intHttp.Get(security.Security_Config_Get, nil)
	var security_config security.SecurityConfig
	err := json.Unmarshal(data, &security_config)
	if err != nil {
		logger.Println("Failed to fetch SecurityInfo: ", err.Error())
	}
	writeCertificates()
	authEnabled := security_config.ClientAuthMethod == auth.MTLS
	updateMosquittoConfig(authEnabled)
	if !authEnabled {
		changeNodeStatus(nodeErrorStatus.WARNING, "SecurityService.security_level not configured for \"AUTHORIZED\". Broker operates in lower level \"AUTHENTICATED\"")
	}
	if !serviceConfig.BrokerRestartCallback() {
		changeNodeStatus(nodeErrorStatus.ERROR, "Restarting Broker failed")
	}
}

func Mqttbroker_Get(p periHttp.PeriHttp) {
	binaryJson, err := json.Marshal(mqttBrokerInfo)
	if err != nil {
		p.JsonResponse(http.StatusInternalServerError, json.RawMessage(`{"error": "`+err.Error()+`"}`))
		return
	}

	p.JsonResponse(http.StatusOK, binaryJson)
}

func Mqttbroker_Config_Get(p periHttp.PeriHttp) {
	content, err := os.ReadFile(serviceConfig.MosquittoUserConfigPath)
	if err != nil {
		p.JsonResponse(http.StatusInternalServerError, json.RawMessage(`{"error": "`+err.Error()+`"}`))
		return
	}

	p.TextResponse(http.StatusOK, content)
}

func Mqttbroker_Config_Set(p periHttp.PeriHttp) {
	payload, _ := p.ReadBody()
	err := shellhelper.WriteFile(serviceConfig.MosquittoUserConfigPath, string(payload))
	if err != nil {
		p.JsonResponse(http.StatusInternalServerError, json.RawMessage(`{"error": "`+err.Error()+`"}`))
	}
	if serviceConfig.BrokerRestartCallback() {
		p.EmptyResponse(http.StatusNoContent)
	} else {
		p.EmptyResponse(http.StatusBadGateway)
		changeNodeStatus(nodeErrorStatus.ERROR, "Restarting Broker failed")
	}
	p.EmptyResponse(http.StatusNoContent)
}

func MQTTBroker_Restart(p periHttp.PeriHttp) {
	if serviceConfig.BrokerRestartCallback() {
		p.EmptyResponse(http.StatusNoContent)
	} else {
		p.EmptyResponse(http.StatusBadGateway)
		changeNodeStatus(nodeErrorStatus.ERROR, "Restarting Broker failed")
	}
}

func Mqttbroker_Log_Get(p periHttp.PeriHttp) {
	content, err := os.ReadFile(serviceConfig.MosquittoLogFilePath)
	if err != nil {
		p.JsonResponse(http.StatusInternalServerError, json.RawMessage(`{"error": "`+err.Error()+`"}`))
		return
	}

	p.TextResponse(http.StatusOK, content)
}

func Mqttbroker_Clients_Get(p periHttp.PeriHttp) {
	clients := ClientsProvider.GetClients()
	binaryJson, err := json.Marshal(clients)

	if err != nil {
		p.JsonResponse(http.StatusInternalServerError, json.RawMessage(`{"error": "`+err.Error()+`"}`))
		return
	}

	p.JsonResponse(http.StatusOK, binaryJson)
}
