/*
Copyright (c) Perinet GmbH
All rights reserved

This software is dual-licensed: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License version 3 as
published by the Free Software Foundation. For the terms of this
license, see http://www.fsf.org/licensing/licenses/agpl-3.0.html,
or contact us at https://perinet.io/contact/ when you want license
this software under a commercial license.
*/

package mosquitto_test

import (
	"encoding/json"
	"net/http"
	"testing"
	"time"

	"gitlab.com/perinet/generic/lib/utils/intHttp"
	"gitlab.com/perinet/generic/lib/utils/shellhelper"
	"gitlab.com/perinet/periMICA-container/apiservice/mosquitto"
	clientsProvider "gitlab.com/perinet/periMICA-container/apiservice/mosquitto/clients"
	"gitlab.com/perinet/periMICA-container/apiservice/node/nodeErrorStatus"
	"gotest.tools/assert"
)

const (
	tmpDir           = "_cache/"
	mosquittoLogFile = tmpDir + "var/log/mosquitto.log"
)

var (
	restartCallback func() bool = nil
)

func init() {
	shellhelper.WriteFile(tmpDir+"/.keep", "")
	restartCallback = func() bool {
		return true
	}
	mosquitto.SetServiceConfig(mosquitto.ServiceConfig{
		StoragePath:             "_cache/var/lib/mosquitto-service/",
		ListenerConfigPath:      "_cache/etc/mosquitto/conf.d/00_mqtt-listener.conf",
		MosquittoUserConfigPath: "_cache/etc/mosquitto/conf.d/conf.d/user.conf",
		MosquittoLogFilePath:    "_cache/var/log/mosquitto.log",
		BrokerRestartCallback: func() bool {
			if restartCallback != nil {
				restartCallback()
				return true
			}
			return false
		},
	})
}

func TestClientsGet(t *testing.T) {
	mosquittoLog := `
	2024-05-31T08:55:00: Client auto-6C16948F-A012-4FD3-5CA2-F81B577C3626 disconnected: Protocol error.
	2024-05-31T08:55:11: New connection from fe80::742e:db1d:d1b2:0:63507 on port 8883.
	2024-05-31T08:55:14: New client connected from fe80::742e:db1d:d1b2:0:63507 as auto-351ABCD3-36F4-3F74-C522-3BBB8E94A855 (p2, c1, k60).
	2024-05-31T09:00:51: Client auto-351ABCD3-36F4-3F74-C522-3BBB8E94A855 disconnected: Protocol error.
	2024-05-31T09:00:54: New connection from fe80::742e:db1d:d1b2:0:63508 on port 8883.
	2024-05-31T09:00:58: New client connected from fe80::742e:db1d:d1b2:0:63508 as auto-B3215F63-12CD-1469-6CD8-B95651BF8270 (p2, c1, k60).
	2024-05-31T09:07:59: mosquitto version 2.0.18 terminating
	2024-05-31T09:08:01: mosquitto version 2.0.18 starting
	2024-05-31T09:08:01: Config loaded from /etc/mosquitto/mosquitto.conf.
	2024-05-31T09:08:01: Opening websockets listen socket on port 8083.
	2024-05-31T09:08:01: Opening ipv6 listen socket on port 8883.
	2024-05-31T09:08:01: mosquitto version 2.0.18 running
	2024-05-31T09:08:01: New connection from fe80::742e:db06:5dec:0:63509 on port 8883.
	2024-05-31T09:08:03: New client connected from fe80::742e:db06:5dec:0:63509 as auto-85FC4C78-1503-FC9E-39E8-B878E6DB94AC (p2, c1, k60).	
	`
	shellhelper.WriteFile(mosquittoLogFile, mosquittoLog)
	time.Sleep(time.Millisecond * 1000)

	data := intHttp.Get(mosquitto.Mqttbroker_Clients_Get, nil)
	var clients []clientsProvider.Client
	err := json.Unmarshal(data, &clients)
	assert.Assert(t, err == nil)
	assert.Assert(t, len(clients) == 1)
}

func TestResetOk(t *testing.T) {
	var callbackfired = false
	restartCallback = func() bool {
		callbackfired = true
		return true
	}
	data := intHttp.Get(mosquitto.Mqttbroker_Get, nil)
	var info mosquitto.MQTTBrokerInfo
	err := json.Unmarshal(data, &info)
	assert.Assert(t, err == nil)
	old_error_status := info.ErrorStatus
	assert.Assert(t, old_error_status != nodeErrorStatus.ERROR)

	response := intHttp.Call(mosquitto.MQTTBroker_Restart, "PATCH", nil, nil)
	assert.Assert(t, response.StatusCode == http.StatusNoContent)

	data = intHttp.Get(mosquitto.Mqttbroker_Get, nil)
	err = json.Unmarshal(data, &info)

	assert.Assert(t, err == nil)
	assert.Assert(t, callbackfired == true)
	assert.Assert(t, info.ErrorStatus == old_error_status)
}

func TestResetFail(t *testing.T) {
	restartCallback = nil

	response := intHttp.Call(mosquitto.MQTTBroker_Restart, "PATCH", nil, nil)
	assert.Assert(t, response.StatusCode == http.StatusBadGateway)

	data := intHttp.Get(mosquitto.Mqttbroker_Get, nil)
	var info mosquitto.MQTTBrokerInfo
	err := json.Unmarshal(data, &info)

	assert.Assert(t, err == nil)
	assert.Assert(t, info.ErrorStatus == nodeErrorStatus.ERROR)
}
