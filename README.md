# API Service mosquitto

The apiservice mosquitto implements the MQTTBroker API for the MQTT broker
`mosquitto`.

> Note: Perinet products implement a minimal set of RESTful API service, so called
> base-services. The specification of those is located in the [unified
> API](https://gitlab.com/perinet/unified-api) repository.


# References

* [openAPI spec for apiservice mqttbroker](ressources/api/MQTTBroker_openapi.yaml)
* [unified
API](https://gitlab.com/perinet/unified-api)
* [License](LICENSE.dual)
