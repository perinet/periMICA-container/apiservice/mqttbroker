module gitlab.com/perinet/periMICA-container/apiservice/mosquitto

go 1.22.0

toolchain go1.22.3

require (
	github.com/godbus/dbus/v5 v5.1.0
	github.com/holoplot/go-avahi v1.0.2-0.20240210093433-b8dc0fc11e7e
	github.com/nxadm/tail v1.4.11
	gitlab.com/perinet/generic/apiservice/dnssd v1.0.1
	gitlab.com/perinet/generic/lib/httpserver v1.0.1
	gitlab.com/perinet/generic/lib/utils v1.0.1
	gitlab.com/perinet/periMICA-container/apiservice/node v1.0.7
	gitlab.com/perinet/periMICA-container/apiservice/security v1.0.5
	gotest.tools v2.2.0+incompatible
	gotest.tools/v3 v3.5.2-0.20240905034822-0b81523ff268
)

require (
	github.com/alexandrevicenzi/go-sse v1.6.0 // indirect
	github.com/felixge/httpsnoop v1.0.4 // indirect
	github.com/fsnotify/fsnotify v1.8.0 // indirect
	github.com/google/go-cmp v0.6.0 // indirect
	github.com/google/uuid v1.6.0 // indirect
	github.com/gorilla/handlers v1.5.2 // indirect
	github.com/gorilla/mux v1.8.1 // indirect
	github.com/grantae/certinfo v0.0.0-20170412194111-59d56a35515b // indirect
	github.com/pkg/errors v0.9.1 // indirect
	golang.org/x/exp v0.0.0-20241108190413-2d47ceb2692f // indirect
	golang.org/x/sys v0.27.0 // indirect
	gopkg.in/tomb.v1 v1.0.0-20141024135613-dd632973f1e7 // indirect
)
